
# EsLint

## Instalación

    cd proyecto

    npm install eslint -D

    npx eslint --init

En caso de error al ejecutar `npx eslint --init` puede ejecutar esta alternativa

    ./node_modules/.bin/eslint --init 

## PLugins VsCode
### plugin vscode EsLint
    https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint

### plugin vscode Error Lens
    https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens


### Configurar fixed con auto-guardado
modificar archivo de configuracion settings.json ubicado en File>Preferences>Settings TextEditor>CodeActionOnSave

`{
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
    "eslint.validate": [
        "javascript",
        "vue",
        "html"
    ],
}`

# Estandard JS

    npm install standard -D

## Importar desde el package.json la configuración
`"eslintConfig": {
    "extends": "./node_modules/standard/eslintrc.json"
}`

