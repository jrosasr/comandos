# Crear nuevo usuario en vps
Solo la contraseña es un campo obligatorio(se debe ejecutar sin sudo si esta con usuarios root)
- `sudo adduser nuevoUsuario`

Asignamos permisos sudo
- `usermod -aG sudo nuevoUsuario`

nos movemos al usuario
- `su – nuevoUsuario`

# Instalar nvm en ubuntu 20.04
- `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash`

Ahora vamos a activar la variable de entorno para NVM con el siguiente comando:
- `source ~/.bashrc`

instalar ultima version de node
- `nvm install node`

Instalar version especifica
- `nvm install 14.18.1`
