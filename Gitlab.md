# Comandos Gitlab

## Subir repositorio local git a Gitlab

- `git init`

- `git add .`

- `git commit -m "Primer commit"`

Se crea un nuevo repositorio para el nuevo proyecto en gitlab y se toma esa url

- `git remote add origin "url_de_repo"`

- `git remote -v`

- `git push -u origin --all`

Este comando elimina el add del remote
- `git remote rm origin`

