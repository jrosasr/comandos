# Manual de instalación del Sistema 

* Entorno: Desarrollo
* Gestor de base de datos: Postgresql
* Usaremos $ para describir los comandos que se ejecutaran con un usuario regular.

## 1) Instalar los siguientes paquetes del sistema operativo:

    php >= 7.4.x
    php-xml
    php-gd
    php-mbstring
    php-tokenizer
    php-zip
    php-cli
    php-curl
    php-pgsql
    postgresql
    postgis
    postgresql-X-postgis-3 (X=versión de postgresql)
    postgresql-X-postgis-3-scripts (X=versión de postgresql)
    curl
    zip
    unzip
    gdal-bin >= 3.3.2

## 2) Descargar e instalar composer de manera global:

Se recomienda seguir la documentación oficial disponible desde: https://getcomposer.org/

## 3) Comprobar la instalación de composer:

Una vez instalado composer, comprobamos la correcta instalación con el siguiente
comando:

    $ composer --version

## 4) Descargar e instalar el paquete nodejs en su última versión:

Se recomienda seguir la documentación disponible desde: https://linuxconfig.org/how-to-install-node-js-on-ubuntu-16-04-xenial-xerus-linux-server pero para este
manual recomendamos los siguientes pasos:

Procedimiento de instalación mediante el script nativo de node **nvm** el cual
permite más control en cuanto a la versión de **node** y **npm** a ser instalada.

Instalar los prerequisitos del S.O. mediante el siguiente comando:

    $ sudo apt install build-essential libssl-dev

Descargar el script de instalación más reciente mediante **curl**, para ello
se requiere ejecutar el comando:

    $ sudo curl -o- https://raw.githubusercontent.com/creationix/nvm/{version}/install.sh | bash

Se debe sustituir la número de versión con la más reciente publicada la
cual se puede encontrar [aquí](https://github.com/nvm-sh/nvm/releases), ejemplo:

    $ sudo curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash

Lo siguiente es configurar el acceso del nuevo script **NVM** con el siguiente
comando:

    $ . ~/.profile

Esto permitira acceder al comando nvm desde cualquier ruta, una vez realizada
la configuración, se procederá a verificar el listado de versiones de **node**
disponibles en el repositorio para lo cual se require ejecutar el comando:

    $ nvm ls-remote | grep -i lts

Esto debe mostrar un listado de versiones de node que correspondan
a las versiones **LTS**, ejemplo:

v10.15.3   (LTS: Dubnium)
v10.16.0   (LTS: Dubnium)
v10.16.1   (LTS: Dubnium)
v10.16.2   (LTS: Dubnium)
v10.16.3   (Latest LTS: Dubnium)

Ahora procedemos a la instalación de la versión de node más actualizada para lo
cual se ejecutará el comando:

    $ nvm install 10.16.3

## 5) Una vez instalado nodejs se debe instalar npm también, comprobamos la correcta instalación con el siguiente comando:

    $ node -v

    $ npm -v

## 6) Acceder al directorio raíz del proyecto:

    $ cd siniif

## 7) Instalar las dependencias de php del proyecto:

    $ composer install

## 8) Instalar las dependencias de nodejs del proyecto:

    $ npm install

## 9) Creamos el archivo de configuración .env a partir de un fichero de ejemplo que nos provee el proyecto:

    $ cp .env.example .env

## 10) Generar un identificador único para la aplicación:

    $ php artisan key:generate

## 11) Configurar las variables de la aplicación:

    APP_NAME=SINIIF
    APP_ENV=production
    APP_DEBUG=false
    APP_URL=url_del_servidor

    CACHE_DRIVER=database
    QUEUE_CONNECTION=database
    SESSION_DRIVER=database
    SESSION_LIFETIME=120

    MAIL_DRIVER=smtp
    MAIL_HOST=host_del_servidor_de_correo
    MAIL_PORT=puerto_del_servidor_de_correo
    MAIL_USERNAME=usuario_de_correo
    MAIL_PASSWORD=password_del_usuario_de_correo
    MAIL_ENCRYPTION=tipo_de_encriptación_del_servidor_de_correo
    MAIL_FROM_ADDRESS=dirección_de_correo
    MAIL_FROM_NAME="${APP_NAME}"

    MIX_APP_URL="${APP_URL}"
    MIX_BASE_URL="${APP_URL}"
    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

Obligatoriamente se deben configurar las variables asociadas al envío de correos
de la aplicación, de lo contrario funcionalidades que requieran enviar correos
como las del módulo de Gestión de usuarios no estarán totalmente funcionales.

## 12) Compilar los archivos javascript y css de la aplicación:

    $ npm run prod

En caso de error de desbordamiento de memoria al compilar, se debe ejecutar el
siguiente comando:

    $ export NODE_OPTIONS=--max-old-space-size=<memoria>

En donde ``<memoria>`` es la cantidad de memoria RAM a configurar, ejemplo:

(1GB)        export NODE_OPTIONS=--max-old-space-size=1024
(2GB)        export NODE_OPTIONS=--max-old-space-size=2048
(3GB)        export NODE_OPTIONS=--max-old-space-size=3072
(4GB)        export NODE_OPTIONS=--max-old-space-size=4096
(5GB)        export NODE_OPTIONS=--max-old-space-size=5120
(6GB)        export NODE_OPTIONS=--max-old-space-size=6144
(7GB)        export NODE_OPTIONS=--max-old-space-size=7168
(8GB)        export NODE_OPTIONS=--max-old-space-size=8192

Una vez configurada la capacidad de memoria, se debe proceder nuevamente con el
comando ``npm run prod``

## 13) Crear una base de datos de postgresql:

Antes de crear la base de datos se debe crear las credenciales de acceso del
usuario que la gestionará, por lo que es necesario ejecutar el comando:

    $ sudo -u postgres psql

Esto dará acceso a la consola de PostgreSQL, una vez allí se debe ejecutar el comando:

    CREATE USER nombre_de_usuario WITH ENCRYPTED PASSWORD 'contraseña_de_acceso' CREATEDB;

La instrucción anterior creará el usuario con el cual interactuar con la base de datos.

Posteriormente se debe ejecutar la siguiente instrucción para crear la base de datos:

    CREATE DATABASE siniif WITH OWNER nombre_de_usuario;

Para poder crear los scripts de PostGIS se debe agregar al usuario recién creado
al grupo de super usuario de postgres para lo cual se debe ejecutar la
instrucción:

    ALTER USER nombre_de_usuario WITH SUPERUSER;

Una vez ejecutadas las instrucciones con el usuario postgres se debe salir de la
consola ejecutando el comando:

    \q

y luego se debe salir del usuario postgres con la instrucción

    exit

Ahora se debe ejecutar la consola con el usuario recién creado, para lo cual se
ejecuta la instrucción:

    psql -U nombre_de_usuario -h servidor_de_base_de_datos -d siniif

Una vez en la base de datos de la aplicación, el siguiente paso es habilitar las
extensiones de PostGIS para la gestión de datos geoespaciales, para lo cual se
deben ejecutar las siguientes instrucciones:

    CREATE EXTENSION postgis;

    CREATE EXTENSION postgis_raster;

    CREATE EXTENSION postgis_topology;

Si en la ejecución de las instrucciones anteriores ocurre algún error, se debe
revisar que el paquete de PostGIS y su respectivo script se encuentre instalado
correctamente.

## 14) Configurar las credenciales de acceso a la base de datos de postgresql en el archivo .env:

    DB_CONNECTION=pgsql
    DB_HOST=localhost
    DB_PORT=5432
    DB_DATABASE=nombre_base_de_datos
    DB_USERNAME=usuario_base_de_datos
    DB_PASSWORD=contrasena_base_de_datos

La base de datos soportada por la aplicación es PostgreSQL >= 9.2

## 15) Limpiar configuración, migrar y cargar la base de datos:

Una vez configurado el gestor de base de datos, se deben ejecutar los siguientes
comandos:

    $ php artisan config:clear

    $ php artisan clear-compiled

    $ php artisan migrate

Los Seeders son componentes del framework Laravel que sirven para cargar datos en
la base de datos, los seeders están almacenados en la ruta siniif/database/seeders/

Cada archivo contiene los datos que se requiere que estén precargados en el
sistema, estos archivos tienen una estructura específica y se ejecutan con el
siguiente comando:

    $ php artisan db:seed

## 16) Configurar la carpeta de archivos estáticos:

Para hacer seguimiento a la carpeta de archivos estáticos se debe ejecutar el
comando:

    $ php artisan storage:link

## 17) Probar la aplicación:

    $ php artisan serve

Este comando levanta un servidor en la dirección ip 127.0.0.1 o localhost y en
el puerto 8000, para verificarlo puedes acceder a el enlace http://127.0.0.1:8000/

Para evitar inconvenientes o errores de origines cruzados de peticiones en la
aplicación al registrar datos, se debe configurar en el archivo .env la url
correcta de la aplicación en la variable APP_URL, bien sea http://127.0.0.1 o
http://localhost según se necesite.

De igual forma se puede levantar el servidor con una dirección IP y un puerto
diferente, esta dirección IP también se debe configurar en la variable APP_URL
del archivo .env:

    $ php artisan serve --host=192.168.0.100 --port=8000

Nota: En la base de datos está registrado un usuario con privilegios de
administrador para poder acceder al sistema, las credenciales son:

    Usuario: admin
    Contraseña: 123456

## 18) Configuración adicional de PHP:

La aplicación requiere de unos ajustes a nivel de configuración del lenguaje
PHP para lo que es necesario editar el archivo php.ini ubicado en la ruta
/etc/php/7.4/apache2 (asumiendo que se encuentre en un servidor apache, de lo
contrario debe indicarse la ruta al servidor de aplicaciones configurado).

Una vez abierto el archivo php.ini para su modificación, se deben buscar las
siguientes variables:

upload_max_filesize, max_file_uploads, post_max_size, max_execution_time y
max_input_time

Estas variables deben contener los siguientes valores (puede modificarse por
valores superiores a los recomendados en esta guía):

-Tamaño máximo de archivos permitidos para cargar al servidor

    upload_max_filesize = 150M

-Número máximo de archivos que se pueden cargar mediante una sola solicitud

    max_file_uploads = 50

-Tamaño máximo de datos a través del método POST

    post_max_size = 150M

-Tiempo máximo de ejecución de cada script, en segundos

    max_execution_time = 60

-Cantidad máxima de tiempo que cada script puede pasar analizando los datos de la solicitud

    max_input_time = 36000

## 19) Permisos de escritura y lectura

La aplicación requiere registrar información en algunas rutas del sistemna de
archivos por lo que es importante darle los permisos necesarios a estas rutas
para la escritura y lectura, para lo cual se debe ejecutar el comando

    $ sudo chmod 775 -R storage/snapshots

Si la carpeta anterior no existe se debe crear con el comando

    $ mkdir storage/snapshots

y posteriormente ejecutar el comando anterior.

Para el registro de eventos se debe dar permisos de escritura y lectura a la
carpeta storage ejecutando el comando

    $ sudo chmod -R 775 storage

## 20) Utilidades extra del sistema

### Cargar información a través de archivo geojson

Si se requiera cargar información en el sistema a través de un archivo con
extesnsión .geojson se deben realizar los siguientes pasos:

-Crear la carpeta "geojson" en el siguiente directorio: siniif/storage/app/,
quedando de esta manera siniif/storage/app/geojson

-Guardar dentro del directorio creado el archivo o archivos con extensión
.geojson que se requieran.

-Ejecutar el siguiente comando:

$ php artisan storage:geojson-import

Este comando lo que hace es abrir e iterar los archivos .geojson que estén
almacenados en el diretorio descrito y cargar su información en el sistema.

En el caso de que se requiera cargar la información de un archivo específico,
el comando a ejecutar sería:

$ php artisan storage:geojson-import --file nombre_archivo.geojson

Nota: La ejecución del proceso de carga del archivo .geojson puede tardar,
dependiendo del tamaño del archivo por lo cual se recomienda que se ejecute
en segundo plano agregando & al final del comando:

$ php artisan storage:geojson-import &

### Cargar información de individuos a través de archivo ODS, XLS y CSV

Si se requiera cargar información de individuos en el sistema a través de un
archivo con extensión .ods, .xls o .csv se deben realizar los siguientes pasos:

-Los archivos trees_import.ods y trees_import_xls.xls se encuentran creados por
defecto en el directorio siniif/storage/app/public/ ambos formatos contienen
comentarios para orientar y ayudar a que la información sea cargada correctamente,
el usuario debe escoger cual de los dos le es más cómodo, pero ambos formatos
funcionan igual.

-Para cargar los datos se debe ejecutar el siguiente comando:

$ php artisan import:trees-import Nombre_del_archivo.extensión_del_archivo

El ejemplo práctico sería:

$ php artisan import:trees-import trees_import.ods

ó

$ php artisan import:trees-import trees_import_xls.xls

La ejecución del comando agrega los individuos de 30, 10, 2.5 y regeneración
natural a la base de datos.

En el caso de usar un archivo con extensión .csv, los archivos DATA_ARB10.csv,
DATA_ARB2.csv, DATA_ARB30.csv y DATA_REGNAT.csv se encuentran creados por
defecto en el directorio siniif/storage/app/public/, cada nombre de estos
archivos corresponde al nombre de cada hoja contenida en los archivos .ods o
.xls debido a que un archivo .csv no permite tener múltiples hojas. Cada archivo
contiene la misma cabecera o primera fila de la hoja correspondiente de los
archivos .ods o .xls.

La ejecución de este archivo sería:

$ php artisan import:trees-import Nombre_del_archivo.csv

El ejemplo práctico sería:

$ php artisan import:trees-import DATA_ARB10.csv

Y se debe ejecutar con cada archivo .csv.

### Notas importantes

-Se recomienda usar un solo archivo que contenga las múltiples hojas, es decir,
usar los formatos .ods o .xls proveídos, de lo contrario, para poder usar el
formato .csv se tendrá que crear un archivo .csv por cada hoja que hay en el
archivo .ods o .xls. El nombre del archivo .csv debe corresponder con el nombre
de la hoja del .ods que se desea importar.

-La funcionalidad de carga a través de este archivo es rigurosa, se rige por los
códigos establecidos en los registros de Datos globales, es decir, las columnas
del archivo que hacen referencia a Datos globales, solo deben tener como valor
el código de la Propiedad.

-Si al intentar insertar un valor en la base de datos no encuentra coincidencia
con lo que está almacenado, el sistema el va ignorar esa fila del archivo.

-Si los códigos del Proyecto o la Parcela no coinciden con los registros del
sistema, se ignora ese registro o fila del archivo.

