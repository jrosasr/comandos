# Guia para multiples versiones de php en linux

## Comandos previos

- `sudo apt install python-software-properties`

- `sudo apt install software-properties-common`

- `sudo add-apt-repository ppa:ondrej/php`

- `sudo apt update`

### Instalar versiones de php deseadas

- `sudo apt install phpX.X`

### Se establece la version deseada

- `sudo update-alternatives --set php /usr/bin/phpX.X`

### Confirmamos el cambio ejecutando

- `php -v`

### En caso de estar usando apache es necesario deshabilitar la version anterior para que apache la reconozca

- `sudo a2dismod php7.4`

- `sudo systemctl restart apache2`

- `sudo a2enmod php8.0`

- `sudo systemctl restart apache2`

### Para verificar que apache reconoce el cambio creamos un archivos **`index.php`** en **`/var/www/html/`** y agregamos el siguiente código

`<?php  phpinfo(); ?>`
